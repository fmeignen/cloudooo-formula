# -*- coding: utf-8 -*-
# vim: ft=sls
{%- from 'cloudooo/map.jinja' import cloudooo with context %}

office_packages:
  pkg.installed:
     - pkgs: 
       - libreoffice-writer
       - libreoffice-calc
       - libreoffice-calc
       - libreoffice-impress
       - libreoffice-draw
       - libreoffice-headless
       - libreoffice

common_packages:
  pkg.latest:
     - pkgs: 
       - python-setuptools
       - python-devel
       - libxslt-devel
       - python-lxml
       - gcc
       - gcc-c++

/etc/init.d/cloudooo:
  file.managed:
    - source: salt://cloudooo/cloudooo
    - hash: md5=1d9dc46ae03a133d0a06bb685387d85a
    - mode: 750

/opt/cloudooo/conf:
  file.directory:
    - makedirs: True
    - recurse:

/opt/cloudooo/pid:
  file.directory:
    - makedirs: True
    - recurse:

/var/log/cloudooo:
  file.directory:
    - makedirs: True

/opt/cloudooo/log:
  file.symlink:
    - target: /var/log/cloudooo

cloudooo_dist:
  archive.extracted:
    - name: /tmp
    - source: {{ cloudooo.dist.url }}
    - archive_format: tar
    - source_hash: {{ cloudooo.dist.hash }}
    - keep: True 
    - if_missing: /tmp/cloudooo-v1.25

/opt/cloudooo/conf/cloudooo.conf:
  file.managed:
    - source: salt://cloudooo/cloudooo.conf
    - hash: md5=c406894c786a2d3c14fd0a24e69b9068

egg_install:
  cmd.run:
    - name: easy_install /tmp/cloudooo-v1.25/*.egg

cloudooo_compil:
  cmd.run:
    - name: cd /tmp/cloudooo-v1.25/cloudooo-v1.25 && python setup.py install

ooo_dist:
  archive.extracted:
    - name: /tmp
    - source: {{ cloudooo.ooo.url }}
    - archive_format: tar
    - source_hash: {{ cloudooo.ooo.hash }}
    - keep: True  
    - if_missing: /tmp/LibreOffice_4.2.6.2_Linux_x86-64_rpm

rpmgnome_clean:
  file.absent:
    - name: /tmp/LibreOffice_4.2.6.2_Linux_x86-64_rpm/RPMS/libobasis4.2-gnome-integration-4.2.6.2-2.x86_64.rpm 

install_rpms:
  cmd.run:
    - name: cd /tmp/LibreOffice_4.2.6.2_Linux_x86-64_rpm/RPMS && yum install -y *.rpm

cloudooo:
  service.running:
    - enable: True
